﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;
using System.Configuration;
using Microsoft.ServiceBus.Notifications;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;

namespace NotificationSender2010
{
    public partial class NotificationSenderForm : Form
    {
        public NotificationSenderForm()
        {
            InitializeComponent();

            fillReceiver();
        }

        private const int UNREAD = 0;

        private void fillReceiver()
        {
            DataTable dtReceivers = getReceivers();

            foreach (DataRow drReceiver in dtReceivers.Rows)
            {
                if (ddlReceiver.Items_FindByText(drReceiver["UserName"].ToString()) == null)
                    ddlReceiver.Items.Add(drReceiver["UserName"].ToString());
            }

            // mockup
            ddlReceiver.SelectedIndex = ddlReceiver.Items.IndexOf(ddlReceiver.Items_FindByText("Erik"));
        }

        private DataTable getReceivers()
        {
            DAL objDal = new DAL();
            DataTable dtReceivers =
                objDal.GetDataQry("SELECT UserName FROM Devices D LEFT JOIN Users U ON D.UserId=U.UserId",
                GetConnectionString()).Tables[0];

            return dtReceivers;
        }

        private string getUserId(string userName)
        {
            DAL objDal = new DAL();
            DataTable dtUsers = objDal.GetDataQry("SELECT UserId FROM Users WHERE UserName = '" + userName + "'",
                GetConnectionString()).Tables[0];

            return dtUsers.Rows[0]["UserId"].ToString();
        }

        private async void SendNotifications(string type)
        {
            MobileServiceClient client =
                new MobileServiceClient(ConfigurationManager.AppSettings["appUri"],
                    ConfigurationManager.AppSettings["appKey"],
                    null);
            string userId = getUserId(ddlReceiver.SelectedItem.ToString());
            DiggerInNotification[] notifications = GetnUserMReMails(userId, type);
            int successCounter = 0;
            int failureCounter = 0;

            foreach (DiggerInNotification din in notifications)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>(2);

                parameters.Add("userid", userId);
                parameters.Add("alert", din.subject);
                parameters.Add("postdata", din.Stringify());

                try
                {
                    JToken result = await client.InvokeApiAsync("sendnotification", System.Net.Http.HttpMethod.Post, parameters);
                    if (result["isSuccessful"].Value<bool>())
                    {
                        successCounter += 1;
                    }
                    else
                    {
                        failureCounter += 1;
                    }
                }
                catch (MobileServiceInvalidOperationException e)
                {
                    txtShortMessage.Text = "Error: " + e.Message;
                    return;
                }

                txtShortMessage.Text = string.Format("{0} successes, {1} failures.", successCounter, failureCounter);
            }

            txtShortMessage.Text = string.Format("{0} successes, {1} failures.", successCounter, failureCounter);
        }

        private DiggerInNotification[] GetnUserMReMails(string userId, string type)
        {
            DAL objDal = new DAL();
            string query =
@"SELECT UMRE.[ID]
      ,[eType]
	  ,[U_MR_ID]
      ,[eSubject]
      ,[eBody]
      ,[eCreatedDate]
      ,[eISRead]
      ,[vName]
      ,[algID]
      ,[AlgShortName] alName
  FROM [dbo].[nUser_MR_eMails] UMRE
  LEFT JOIN [dbo].[nsfAlgorithmsMaster] AM ON AM.[ID]=UMRE.[algID]
  WHERE [eType]='" + type +
                   "' AND [UserID]='" + userId +
                   "' AND [eISRead] = " + UNREAD;
            DataSet dsEmails =
                objDal.GetDataQry(query, GetConnectionString());

            if (dsEmails != null && dsEmails.Tables.Count > 0)
            {
                List<DiggerInNotification> notifications = new List<DiggerInNotification>();

                foreach (DataRow row in dsEmails.Tables["table"].Rows)
                {
                    notifications.Add(new DiggerInNotification(row));
                }

                return notifications.ToArray();
            }

            return null;
        }

        private StringCollection getDevices(string userId)
        {
            DAL objDal = new DAL();
            string sql = "SELECT * FROM Devices WHERE UserId = '" + userId + "'";
            DataTable dt = objDal.GetDataQry(sql, GetConnectionString()).Tables["Table"];
            StringCollection devices = new StringCollection();

            foreach (DataRow drDevices in dt.Rows)
            {
                devices.Add(drDevices["DeviceId"].ToString());
            }

            return devices;
        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString;
        }

        #region Button clicks
        protected void btnSendAlerts_Click(object sender, EventArgs e)
        {
            txtShortMessage.Text = "";
            SendNotifications("Alert");
        }

        protected void btnSendNotifications_Click(object sender, EventArgs e)
        {
            txtShortMessage.Text = "";
            SendNotifications("Periodic Notification");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtShortMessage.Text = "";
        }
        #endregion
    }
}
