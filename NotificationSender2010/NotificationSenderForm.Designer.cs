﻿namespace NotificationSender2010
{
    partial class NotificationSenderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotificationSenderForm));
            this.txtShortMessage = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSendAlerts = new System.Windows.Forms.Button();
            this.btenSendnotifications = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.ddlReceiver = new NotificationSender2010.TextComboBox();
            this.SuspendLayout();
            // 
            // txtShortMessage
            // 
            this.txtShortMessage.Location = new System.Drawing.Point(88, 39);
            this.txtShortMessage.Name = "txtShortMessage";
            this.txtShortMessage.Size = new System.Drawing.Size(188, 20);
            this.txtShortMessage.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Short message";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Receiver";
            // 
            // btnSendAlerts
            // 
            this.btnSendAlerts.Location = new System.Drawing.Point(88, 65);
            this.btnSendAlerts.Name = "btnSendAlerts";
            this.btnSendAlerts.Size = new System.Drawing.Size(75, 23);
            this.btnSendAlerts.TabIndex = 4;
            this.btnSendAlerts.Text = "Send alerts";
            this.btnSendAlerts.UseVisualStyleBackColor = true;
            this.btnSendAlerts.Click += new System.EventHandler(this.btnSendAlerts_Click);
            // 
            // btenSendnotifications
            // 
            this.btenSendnotifications.Location = new System.Drawing.Point(169, 65);
            this.btenSendnotifications.Name = "btenSendnotifications";
            this.btenSendnotifications.Size = new System.Drawing.Size(107, 23);
            this.btenSendnotifications.TabIndex = 5;
            this.btenSendnotifications.Text = "Send notifications";
            this.btenSendnotifications.UseVisualStyleBackColor = true;
            this.btenSendnotifications.Click += new System.EventHandler(this.btnSendNotifications_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(282, 37);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(44, 23);
            this.btnClear.TabIndex = 6;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ddlReceiver
            // 
            this.ddlReceiver.FormattingEnabled = true;
            this.ddlReceiver.Location = new System.Drawing.Point(88, 12);
            this.ddlReceiver.Name = "ddlReceiver";
            this.ddlReceiver.Size = new System.Drawing.Size(121, 21);
            this.ddlReceiver.TabIndex = 2;
            // 
            // NotificationSenderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 108);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btenSendnotifications);
            this.Controls.Add(this.btnSendAlerts);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ddlReceiver);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtShortMessage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NotificationSenderForm";
            this.Text = "Notification Sender 2010";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtShortMessage;
        private System.Windows.Forms.Label label1;
        private TextComboBox ddlReceiver;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSendAlerts;
        private System.Windows.Forms.Button btenSendnotifications;
        private System.Windows.Forms.Button btnClear;
    }
}

