﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web;

namespace NotificationSender2010
{
    public class DiggerInNotification
    {
        public int emailId { get; private set; }
        public string type { get; private set; }
        public int uMrId { get; private set; }
        public string subject { get; private set; }
        public long createdDate { get; private set; }
        public bool isRead { get; private set; }
        public string vName { get; private set; }
        public int algId { get; private set; }
        public string alName { get; private set; }

        public DiggerInNotification()
        { }

        public DiggerInNotification(DataRow row)
        {
            emailId = int.Parse(row["ID"].ToString());
            type = row["eType"].ToString();
            uMrId = int.Parse(row["U_MR_ID"].ToString());
            subject = row["eSubject"].ToString();
            createdDate = (long) (DateTime.Parse(row["eCreatedDate"].ToString(), new CultureInfo("no")) - UNIX_EPOCH).TotalMilliseconds;
            isRead = (bool) row["eISRead"];
            vName = row["vName"].ToString();
            algId = int.Parse(row["algID"].ToString());
            alName = row["alName"].ToString();
        }

        public string Stringify()
        {
            string postData =
                "\"emailId\": \"" + emailId + "\"" +
                ",\"eType\": \"" + type + "\"" +
                ",\"uMRId\": \"" + uMrId + "\"" +
                ",\"eSubject\": \"" + subject + "\"" +
                ",\"eIsRead\": \"" + isRead + "\"" +
                ",\"date\": \"" + createdDate + "\"" +
                ",\"vName\": \"" + vName + "\"" +
                ",\"algId\": \"" + algId + "\"" +
                ",\"alName\": \"" + alName + "\"";

            return postData;
        }

        /// <summary>
        /// Constant to convert from C# Epoch (0001-01-01) to Java Epoch (1970-01-01)
        /// </summary>
        private readonly DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0);
    }
}