﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NotificationSender2010
{
    public class TextComboBox : ComboBox
    {
        public Object Items_FindByText(string textToFind)
        {
            foreach (Object item in Items)
            {
                if (item.ToString() == textToFind)
                {
                    return item;
                }
            }

            return null;
        }
    }
}
